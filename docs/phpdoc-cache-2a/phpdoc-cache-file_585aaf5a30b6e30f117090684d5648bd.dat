O:39:"phpDocumentor\Descriptor\FileDescriptor":22:{s:7:" * hash";s:32:"0b8867911d2ade4c4ae4f9778eeb382a";s:7:" * path";s:64:"vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php";s:9:" * source";s:6790:"<?php

namespace PhpOffice\PhpSpreadsheet;

use PhpOffice\PhpSpreadsheet\Shared\File;

/**
 * Factory to create readers and writers easily.
 *
 * It is not required to use this class, but it should make it easier to read and write files.
 * Especially for reading files with an unknown format.
 */
abstract class IOFactory
{
    private static $readers = [
        'Xlsx' => Reader\Xlsx::class,
        'Xls' => Reader\Xls::class,
        'Xml' => Reader\Xml::class,
        'Ods' => Reader\Ods::class,
        'Slk' => Reader\Slk::class,
        'Gnumeric' => Reader\Gnumeric::class,
        'Html' => Reader\Html::class,
        'Csv' => Reader\Csv::class,
    ];

    private static $writers = [
        'Xls' => Writer\Xls::class,
        'Xlsx' => Writer\Xlsx::class,
        'Ods' => Writer\Ods::class,
        'Csv' => Writer\Csv::class,
        'Html' => Writer\Html::class,
        'Tcpdf' => Writer\Pdf\Tcpdf::class,
        'Dompdf' => Writer\Pdf\Dompdf::class,
        'Mpdf' => Writer\Pdf\Mpdf::class,
    ];

    /**
     * Create Writer\IWriter.
     *
     * @param Spreadsheet $spreadsheet
     * @param string $writerType Example: Xlsx
     *
     * @throws Writer\Exception
     *
     * @return Writer\IWriter
     */
    public static function createWriter(Spreadsheet $spreadsheet, $writerType)
    {
        if (!isset(self::$writers[$writerType])) {
            throw new Writer\Exception("No writer found for type $writerType");
        }

        // Instantiate writer
        $className = self::$writers[$writerType];

        return new $className($spreadsheet);
    }

    /**
     * Create Reader\IReader.
     *
     * @param string $readerType Example: Xlsx
     *
     * @throws Reader\Exception
     *
     * @return Reader\IReader
     */
    public static function createReader($readerType)
    {
        if (!isset(self::$readers[$readerType])) {
            throw new Reader\Exception("No reader found for type $readerType");
        }

        // Instantiate reader
        $className = self::$readers[$readerType];

        return new $className();
    }

    /**
     * Loads Spreadsheet from file using automatic Reader\IReader resolution.
     *
     * @param string $pFilename The name of the spreadsheet file
     *
     * @throws Reader\Exception
     *
     * @return Spreadsheet
     */
    public static function load($pFilename)
    {
        $reader = self::createReaderForFile($pFilename);

        return $reader->load($pFilename);
    }

    /**
     * Identify file type using automatic Reader\IReader resolution.
     *
     * @param string $pFilename The name of the spreadsheet file to identify
     *
     * @throws Reader\Exception
     *
     * @return string
     */
    public static function identify($pFilename)
    {
        $reader = self::createReaderForFile($pFilename);
        $className = get_class($reader);
        $classType = explode('\\', $className);
        unset($reader);

        return array_pop($classType);
    }

    /**
     * Create Reader\IReader for file using automatic Reader\IReader resolution.
     *
     * @param string $filename The name of the spreadsheet file
     *
     * @throws Reader\Exception
     *
     * @return Reader\IReader
     */
    public static function createReaderForFile($filename)
    {
        File::assertFile($filename);

        // First, lucky guess by inspecting file extension
        $guessedReader = self::getReaderTypeFromExtension($filename);
        if ($guessedReader !== null) {
            $reader = self::createReader($guessedReader);

            // Let's see if we are lucky
            if (isset($reader) && $reader->canRead($filename)) {
                return $reader;
            }
        }

        // If we reach here then "lucky guess" didn't give any result
        // Try walking through all the options in self::$autoResolveClasses
        foreach (self::$readers as $type => $class) {
            //    Ignore our original guess, we know that won't work
            if ($type !== $guessedReader) {
                $reader = self::createReader($type);
                if ($reader->canRead($filename)) {
                    return $reader;
                }
            }
        }

        throw new Reader\Exception('Unable to identify a reader for this file');
    }

    /**
     * Guess a reader type from the file extension, if any.
     *
     * @param string $filename
     *
     * @return null|string
     */
    private static function getReaderTypeFromExtension($filename)
    {
        $pathinfo = pathinfo($filename);
        if (!isset($pathinfo['extension'])) {
            return null;
        }

        switch (strtolower($pathinfo['extension'])) {
            case 'xlsx': // Excel (OfficeOpenXML) Spreadsheet
            case 'xlsm': // Excel (OfficeOpenXML) Macro Spreadsheet (macros will be discarded)
            case 'xltx': // Excel (OfficeOpenXML) Template
            case 'xltm': // Excel (OfficeOpenXML) Macro Template (macros will be discarded)
                return 'Xlsx';
            case 'xls': // Excel (BIFF) Spreadsheet
            case 'xlt': // Excel (BIFF) Template
                return 'Xls';
            case 'ods': // Open/Libre Offic Calc
            case 'ots': // Open/Libre Offic Calc Template
                return 'Ods';
            case 'slk':
                return 'Slk';
            case 'xml': // Excel 2003 SpreadSheetML
                return 'Xml';
            case 'gnumeric':
                return 'Gnumeric';
            case 'htm':
            case 'html':
                return 'Html';
            case 'csv':
                // Do nothing
                // We must not try to use CSV reader since it loads
                // all files including Excel files etc.
                return null;
            default:
                return null;
        }
    }

    /**
     * Register a writer with its type and class name.
     *
     * @param string $writerType
     * @param string $writerClass
     */
    public static function registerWriter($writerType, $writerClass)
    {
        if (!is_a($writerClass, Writer\IWriter::class, true)) {
            throw new Writer\Exception('Registered writers must implement ' . Writer\IWriter::class);
        }

        self::$writers[$writerType] = $writerClass;
    }

    /**
     * Register a reader with its type and class name.
     *
     * @param string $readerType
     * @param string $readerClass
     */
    public static function registerReader($readerType, $readerClass)
    {
        if (!is_a($readerClass, Reader\IReader::class, true)) {
            throw new Reader\Exception('Registered readers must implement ' . Reader\IReader::class);
        }

        self::$readers[$readerType] = $readerClass;
    }
}
";s:19:" * namespaceAliases";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:4:"File";s:37:"\PhpOffice\PhpSpreadsheet\Shared\File";}}s:11:" * includes";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:12:" * constants";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:12:" * functions";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:10:" * classes";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:35:"\PhpOffice\PhpSpreadsheet\IOFactory";O:40:"phpDocumentor\Descriptor\ClassDescriptor":19:{s:9:" * parent";s:0:"";s:13:" * implements";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:11:" * abstract";b:1;s:8:" * final";b:0;s:12:" * constants";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:13:" * properties";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:7:"readers";O:43:"phpDocumentor\Descriptor\PropertyDescriptor":16:{s:9:" * parent";r:16;s:8:" * types";N;s:10:" * default";s:451:"array('Xlsx' => \PhpOffice\PhpSpreadsheet\Reader\Xlsx::class, 'Xls' => \PhpOffice\PhpSpreadsheet\Reader\Xls::class, 'Xml' => \PhpOffice\PhpSpreadsheet\Reader\Xml::class, 'Ods' => \PhpOffice\PhpSpreadsheet\Reader\Ods::class, 'Slk' => \PhpOffice\PhpSpreadsheet\Reader\Slk::class, 'Gnumeric' => \PhpOffice\PhpSpreadsheet\Reader\Gnumeric::class, 'Html' => \PhpOffice\PhpSpreadsheet\Reader\Html::class, 'Csv' => \PhpOffice\PhpSpreadsheet\Reader\Csv::class)";s:9:" * static";b:1;s:13:" * visibility";s:7:"private";s:8:" * fqsen";s:44:"\PhpOffice\PhpSpreadsheet\IOFactory::readers";s:7:" * name";s:7:"readers";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:15;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:3:"var";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:40:"phpDocumentor\Descriptor\Validator\Error":4:{s:11:" * severity";s:5:"error";s:7:" * code";s:13:"PPC:ERR-50007";s:7:" * line";i:15;s:10:" * context";a:1:{i:0;s:8:"$readers";}}}}s:19:" * inheritedElement";N;}s:7:"writers";O:43:"phpDocumentor\Descriptor\PropertyDescriptor":16:{s:9:" * parent";r:16;s:8:" * types";N;s:10:" * default";s:465:"array('Xls' => \PhpOffice\PhpSpreadsheet\Writer\Xls::class, 'Xlsx' => \PhpOffice\PhpSpreadsheet\Writer\Xlsx::class, 'Ods' => \PhpOffice\PhpSpreadsheet\Writer\Ods::class, 'Csv' => \PhpOffice\PhpSpreadsheet\Writer\Csv::class, 'Html' => \PhpOffice\PhpSpreadsheet\Writer\Html::class, 'Tcpdf' => \PhpOffice\PhpSpreadsheet\Writer\Pdf\Tcpdf::class, 'Dompdf' => \PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf::class, 'Mpdf' => \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class)";s:9:" * static";b:1;s:13:" * visibility";s:7:"private";s:8:" * fqsen";s:44:"\PhpOffice\PhpSpreadsheet\IOFactory::writers";s:7:" * name";s:7:"writers";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:26;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:3:"var";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:40:"phpDocumentor\Descriptor\Validator\Error":4:{s:11:" * severity";s:5:"error";s:7:" * code";s:13:"PPC:ERR-50007";s:7:" * line";i:26;s:10:" * context";a:1:{i:0;s:8:"$writers";}}}}s:19:" * inheritedElement";N;}}}s:10:" * methods";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:8:{s:12:"createWriter";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:16;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:1;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:12:"$spreadsheet";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:82;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:37:"\PhpOffice\PhpSpreadsheet\Spreadsheet";}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:12:"$spreadsheet";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:11:"$writerType";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:82;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:11:"$writerType";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:13:"Example: Xlsx";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:51:"\PhpOffice\PhpSpreadsheet\IOFactory::createWriter()";s:7:" * name";s:12:"createWriter";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:22:"Create Writer\IWriter.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:47;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:3:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:12:"$spreadsheet";s:8:" * types";r:92;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}i:1;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:11:"$writerType";s:8:" * types";r:114;s:7:" * name";s:5:"param";s:14:" * description";s:13:"Example: Xlsx";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"throws";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ThrowsDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:42:"\PhpOffice\PhpSpreadsheet\Writer\Exception";}}}s:7:" * name";s:6:"throws";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:40:"\PhpOffice\PhpSpreadsheet\Writer\IWriter";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:12:"createReader";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:16;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:1;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:11:"$readerType";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:184;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:11:"$readerType";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:13:"Example: Xlsx";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:51:"\PhpOffice\PhpSpreadsheet\IOFactory::createReader()";s:7:" * name";s:12:"createReader";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:22:"Create Reader\IReader.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:68;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:3:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:11:"$readerType";s:8:" * types";r:194;s:7:" * name";s:5:"param";s:14:" * description";s:13:"Example: Xlsx";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"throws";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ThrowsDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:42:"\PhpOffice\PhpSpreadsheet\Reader\Exception";}}}s:7:" * name";s:6:"throws";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:40:"\PhpOffice\PhpSpreadsheet\Reader\IReader";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:4:"load";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:16;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:1;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:10:"$pFilename";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:257;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:10:"$pFilename";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:32:"The name of the spreadsheet file";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:43:"\PhpOffice\PhpSpreadsheet\IOFactory::load()";s:7:" * name";s:4:"load";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:70:"Loads Spreadsheet from file using automatic Reader\IReader resolution.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:89;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:3:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:10:"$pFilename";s:8:" * types";r:267;s:7:" * name";s:5:"param";s:14:" * description";s:32:"The name of the spreadsheet file";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"throws";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ThrowsDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:42:"\PhpOffice\PhpSpreadsheet\Reader\Exception";}}}s:7:" * name";s:6:"throws";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:37:"\PhpOffice\PhpSpreadsheet\Spreadsheet";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:8:"identify";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:16;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:1;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:10:"$pFilename";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:330;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:10:"$pFilename";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:44:"The name of the spreadsheet file to identify";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:47:"\PhpOffice\PhpSpreadsheet\IOFactory::identify()";s:7:" * name";s:8:"identify";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:61:"Identify file type using automatic Reader\IReader resolution.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:105;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:3:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:10:"$pFilename";s:8:" * types";r:340;s:7:" * name";s:5:"param";s:14:" * description";s:44:"The name of the spreadsheet file to identify";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"throws";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ThrowsDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:42:"\PhpOffice\PhpSpreadsheet\Reader\Exception";}}}s:7:" * name";s:6:"throws";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:19:"createReaderForFile";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:16;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:1;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:9:"$filename";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:402;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:9:"$filename";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:32:"The name of the spreadsheet file";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:58:"\PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile()";s:7:" * name";s:19:"createReaderForFile";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:73:"Create Reader\IReader for file using automatic Reader\IReader resolution.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:124;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:3:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:9:"$filename";s:8:" * types";r:412;s:7:" * name";s:5:"param";s:14:" * description";s:32:"The name of the spreadsheet file";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"throws";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ThrowsDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:42:"\PhpOffice\PhpSpreadsheet\Reader\Exception";}}}s:7:" * name";s:6:"throws";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:40:"\PhpOffice\PhpSpreadsheet\Reader\IReader";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:26:"getReaderTypeFromExtension";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:16;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:1;s:13:" * visibility";s:7:"private";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:9:"$filename";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:475;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:9:"$filename";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:65:"\PhpOffice\PhpSpreadsheet\IOFactory::getReaderTypeFromExtension()";s:7:" * name";s:26:"getReaderTypeFromExtension";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:52:"Guess a reader type from the file extension, if any.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:161;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:9:"$filename";s:8:" * types";r:485;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:4:"null";}i:1;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:14:"registerWriter";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:16;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:1;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:11:"$writerType";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:538;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:11:"$writerType";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:12:"$writerClass";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:538;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:12:"$writerClass";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:53:"\PhpOffice\PhpSpreadsheet\IOFactory::registerWriter()";s:7:" * name";s:14:"registerWriter";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:47:"Register a writer with its type and class name.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:205;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:11:"$writerType";s:8:" * types";r:548;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}i:1;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:12:"$writerClass";s:8:" * types";r:569;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:14:"registerReader";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:16;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:1;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:11:"$readerType";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:619;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:11:"$readerType";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:12:"$readerClass";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:619;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:12:"$readerClass";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:53:"\PhpOffice\PhpSpreadsheet\IOFactory::registerReader()";s:7:" * name";s:14:"registerReader";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:47:"Register a reader with its type and class name.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:220;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:11:"$readerType";s:8:" * types";r:629;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}i:1;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:12:"$readerClass";s:8:" * types";r:650;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:13:" * usedTraits";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:35:"\PhpOffice\PhpSpreadsheet\IOFactory";s:7:" * name";s:9:"IOFactory";s:12:" * namespace";s:25:"\PhpOffice\PhpSpreadsheet";s:10:" * package";s:0:"";s:10:" * summary";s:45:"Factory to create readers and writers easily.";s:14:" * description";s:144:"It is not required to use this class, but it should make it easier to read and write files.
Especially for reading files with an unknown format.";s:17:" * fileDescriptor";r:1;s:7:" * line";i:13;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:7:"package";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:38:"phpDocumentor\Descriptor\TagDescriptor":3:{s:7:" * name";s:7:"package";s:14:" * description";s:7:"Default";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:10:"subpackage";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:13:" * interfaces";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * traits";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:10:" * markers";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:0:"";s:7:" * name";s:13:"IOFactory.php";s:12:" * namespace";N;s:10:" * package";s:7:"Default";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:7:"package";r:712;s:10:"subpackage";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:40:"phpDocumentor\Descriptor\Validator\Error":4:{s:11:" * severity";s:5:"error";s:7:" * code";s:13:"PPC:ERR-50000";s:7:" * line";i:0;s:10:" * context";a:2:{s:11:"{{ value }}";s:2:"""";i:0;s:0:"";}}}}s:19:" * inheritedElement";N;}