<?php

/**
 * Class Teacheer
 * @package task_20
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_20;

/**
 * Class Teacher
 * @package task_20
 */
class Teacher extends Student
{
    /**
     * Function validator
     * @param $name
     * @param $surname
     * @param $patronymic
     * @return bool
     */
    public function validator($name, $surname, $patronymic)
    {
        if(parent::validator($name, $surname) && ctype_alpha($patronymic)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Function serCourseAdministrator
     * @param $name
     * @param $surname
     * @param $patronymic
     */
    public function setCourseAdministrator($name, $surname, $patronymic): void
    {
        if($this->validator($name, $surname, $patronymic)) {
            $this->courseAdministratorName = $name;
            $this->courseAdministratorSurname = $surname;
            $this->courseAdministratorPatronymic = $patronymic;
        }else{
            $this->courseAdministratorName = 'Error';
            $this->courseAdministratorSurname = 'Error';
        }
    }
}
