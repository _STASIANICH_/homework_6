<?php

/**
 * Class Student
 * @package task_20
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_20;

/**
 * Class Student
 * @package task_20
 */
class Student
{
    /**
     * Property name
     * @var string
     */
    private $name;

    /**
     * Property course
     * @var int
     */
    private $course = 3;

    /**
     * Property courseAdministratorName
     * @var string
     */
    protected $courseAdministratorName;

    /**
     * Property courseAdministratorSurname
     * @var string
     */
    protected $courseAdministratorSurname;

    /**
     * Property courseAdministratorPatronymic
     * @var string
     */
    protected $courseAdministratorPatronymic;


    /**
     * Function transferToNextCourse
     */
    public function transferToNextCourse(): void
    {
        if($this->isCourseCorrect()){
            $this->course++;
        }
    }

    /**
     * Function isCourseCorrect
     * @return bool
     */
    private function isCourseCorrect(): bool
    {
        if (($this->course + 1) < 6) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function setCourseAdministrator
     * @param $adminName
     * @param $adminSurname
     */
    public function setCourseAdministrator($adminName, $adminSurname): void
    {
        if($this->validator($adminName, $adminSurname)) {
            $this->courseAdministratorName = $adminName;
            $this->courseAdministratorSurname = $adminSurname;
        }else{
            $this->courseAdministratorName = 'Error';
            $this->courseAdministratorSurname = 'Error';
        }
    }

    /**
     * Function validator
     * @param $name
     * @param $surname
     * @return bool
     */
    protected function validator($name, $surname): bool
    {
        if(ctype_alpha($name) && ctype_alpha($surname)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Function getCourseAdministrator
     */
    public function getCourseAdministrator()
    {
        echo $this->courseAdministratorName;
        echo ' ' . $this->courseAdministratorPatronymic;
        echo ' ' . $this->courseAdministratorSurname . '<br>';
    }
}
