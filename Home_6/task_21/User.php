<?php

/**
 * Class User
 * @package task_21
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_21;

/**
 * Class User
 * @package task_21
 */
class User
{
    /**
     * Property name
     * @var string
     */
    private $name;

    /**
     * Property surname
     * @var string
     */
    private $surname;

    /**
     * Property birthday
     * @var string
     */
    private $birthday;

    /**
     * Property age
     * @var false|int|mixed|string
     */
    private $age;

    /**
     * User constructor.
     * @param string $name
     * @param string $surname
     * @param $birthday
     */
    public function __construct($name = 'Roman', $surname = 'Romanov', $birthday)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->birthday = $birthday;
        $this->age = $this->calculateAge($birthday);
    }

    /**
     * Function getName
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Function getSurname
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * Function getBirthday
     * @return string
     */
    public function getBirthday(): string
    {
        return $this->birthday;
    }

    /**
     * Function getAge
     * @return float
     */
    public function getAge(): float
    {
        return $this->age;
    }

    /**
     * Function calculateAge
     * @param $birthday
     * @return false|int|mixed|string
     */
    private function calculateAge($birthday)
    {
        $arr = explode('-', $birthday);
        $year = $arr[0];
        $month = $arr[1];
        $day = $arr[2];
        if ($month > date('m') || $month == date('m') && $day > date('d')) {
            return (date('Y') - $year - 1);
        } else {
            return (date('Y') - $year);
        }
    }
}
