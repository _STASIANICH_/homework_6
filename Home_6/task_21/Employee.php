<?php

/**
 * Class Employee
 * @package task_21
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_21;

/**
 * Class Employee
 * @package task_21
 */
class Employee extends User
{
    /**
     * Property salary
     * @var float
     */
    private $salary;

    /**
     * Employee constructor.
     * @param $name
     * @param $surname
     * @param $birthday
     * @param $salary
     */
    public function __construct($name, $surname, $birthday, $salary)
    {
        parent::__construct($name, $surname, $birthday);
        $this->salary = $salary;
    }

    /**
     * Function getSalary
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }
}
