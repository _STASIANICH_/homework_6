<?php

/**
 * Class Employee
 * @package task_9
 * @author Pechenyi Stanislav
 * @version 1.0.1 (16/02/2020)
 */

declare(strict_types=1);

namespace task_9;

/**
 * Class Employee
 * @package task_9
 */
class Employee
{
    /**
     * Property name
     * @var string
     */
    private  $name = 'Vasya';

    /**
     * Property age
     * @var int
     */
    private  $age;

    /**
     * Property salary
     * @var float
     */
    private  $salary = 500;

    /**
     * Function setAge
     * @param $age
     */
    public function setAge($age): void
    {
        if ($this->isAgeCorrect($age)) {
            $this->age = $age;
        }
    }

    /**
     * Function getSalary
     * @return string
     */
    public function getSalary(): string
    {
        return $this->salary . ' $';
    }

    /**
     * Function isAgeCorrect
     * @param $age
     * @return bool
     */
    private function isAgeCorrect($age): bool
    {
        if ($age >= 1 && $age <= 100) {
            return true;
        } else {
            return false;
        }
    }
}
