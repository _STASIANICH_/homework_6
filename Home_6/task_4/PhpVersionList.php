<?php

/**
 * Class PhpVersionList
 * @package task_4
 * @author Pechenyi Stanislav
 * @version 1.0.1 (14/02/2020)
 */

declare(strict_types=1);

namespace task_4;

/**
 * Class PhpVersionList
 * @package task_4
 */
class PhpVersionList extends \SplQueue
{
    /**
     * @return mixed
     */
    public function getLastElement(){
        return parent::top();
    }
}
