<?php
/**
 * Class Employee
 *
 * @package task_18
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_18;

/**
 * Class Employee
 * @package task_18
 */
class Employee
{
    /**
     * Property name
     * @var string
     */
    private $name;

    /**
     * Property surname
     * @var string
     */
    private $surname;

    /**
     * Property post
     * @var Post
     */
    public $post;

    /**
     * Employee constructor.
     * @param string $name
     * @param string $surname
     * @param Post $post
     */
    public function __construct($name = 'Vasya', $surname = "Pupkin", Post $post)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->post = $post;
    }

    /**
     * Function setName
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Function getName
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Function setSurname
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * Function getSurname
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * Function changePsot
     * @param $post
     */
    public function changePost(Post $post): void
    {
        $this->post = $post;
    }
}
