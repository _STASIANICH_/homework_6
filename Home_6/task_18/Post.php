<?php
/**
 * Class Post
 *
 * @package task_18
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_18;

/**
 * Class Post
 * @package task_18
 */
class Post
{
    /**
     * Property name
     * @var string
     */
    private $name;

    /**
     * Property salary
     * @var float
     */
    private $salary;

    /**
     * Post constructor.
     * @param string $name
     * @param float $salary
     */
    public function __construct(string $name, float $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }

    /**
     * Function getName
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Function getSalary
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }
}
