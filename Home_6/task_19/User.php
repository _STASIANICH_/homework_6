<?php

/**
 * Class User
 * @package task_19
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_19;

/**
 * Class User
 * @package task_19
 */
class User
{
    /**
     * Property name
     * @var string
     */
    private $name;

    /**
     * Property age
     * @var int
     */
    protected $age;

    /**добавил новое свойство*/

    /**
     * Function set
     * @param $name
     * @param $value
     */
    public function __set($name, $value): void
    {
        if (!isset($this->$name)) {
            $this->$name = $value;
        }
    }

    /**
     * Function get
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * Function setAgeV1
     * @param $age
     */
    protected function setAgeV1($age): void
    {
        $this->age = $age;
    }

    /**
     * Function setAgeV2
     * @param $age
     */
    public function setAgeV2($age): void
    {
        if($age >= 18) {
            $this->age = $age;
        }
    }
}
