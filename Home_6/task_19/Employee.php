<?php

/**
 * Class Employee
 * @package task_19
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_19;

/**
 * Class Employee
 * @package task_19
 */
class Employee extends User
{
    /**
     * Property salary
     * @var float
     */
    protected $salary;

    /**
     * Function setSalary
     * @param $salary
     */
    protected function setSalery($salary): void
    {
        $this->salary = $salary;
    }

    /**
     * Function getSalary
     * @return float
     */
    protected function getSalery(): float
    {
        return $this->salary;
    }
}
