<?php

/**
 * Class Driver
 * @package task_19
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_19;

/**
 * Class Driver
 * @package task_19
 */
class Driver extends Employee
{
    /**
     * Property experience
     * @var float
     */
    private $experience;

    /**
     * Property category
     * @var string
     */
    private $category;

    /**
     * Function getExperience
     * @return float
     */
    public function getExperience(): float
    {
        return $this->experience;
    }

    /**
     * Function setExperience
     * @param float $experience
     */
    public function setExperience(float $experience): void
    {
        $this->experience = $experience;
    }

    /**
     * Function getCategory
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * Function setCategory
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }
}
