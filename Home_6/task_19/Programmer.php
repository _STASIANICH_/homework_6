<?php

/**
 * Class Programmer
 * @package task_19
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_19;

/**
 * Class Programmer
 * @package task_19
 */
class Programmer extends Employee
{
    /**
     * Property langs
     * @var array
     */
    private $langs = [];

    /**
     * Function getlang
     * @return array
     */
    public function getLang()
    {
        return $this->langs;
    }

    /**
     * Function setLang
     * @param string $newLang
     */
    public function setLang(string $newLang): void
    {
        $this->langs[] = $newLang;
    }
}
