<?php

/**
 * Class User
 *
 * @package task_17
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_17;

/**
 * Class User
 * @package task_17
 */
class User
{
    /**
     * Property name
     * @var string
     */
    public $name;

    /**
     * Property surname
     * @var string
     */
    public $surname;

    /**
     * User constructor.
     * @param $name
     * @param $surname
     */
    public function __construct($name, $surname)
    {
        $this->name = $name;
        $this->surname = $surname;
    }
}
