<?php

/**
 * Class Employee
 * @package task_17
 * @author Pechenyi Stanislav
 * @todo change public property into private for $salary
 */

declare(strict_types=1);

namespace task_17;

/**
 * Class Employee
 * @package task_17
 */

class Employee extends User
{
    /**
     * Property salary
     * @var float
     */
    public $salary;

    /**
     * Employee constructor.
     * @param $name
     * @param $surname
     * @param $salary
     */
    public function __construct($name, $surname, $salary)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->salary = $salary;
    }
}
