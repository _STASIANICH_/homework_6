<?php

/**
 * Class City
 * @package task_17
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_17;

/**
 * Class City
 * @package task_17
 */
class City
{
    /**
     * Property name
     * @var string
     */
    public $name;

    /**
     * Property population
     * @var float
     */
    public $population;

    /**
     * City constructor.
     * @param $name
     * @param $population
     */
    public function __construct($name, $population)
    {
        $this->name = $name;
        $this->population = $population;
    }
}
