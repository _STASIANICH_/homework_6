<?php

/**
 * Interface UserInterface
 * @package task_24
 * @author Pechenyi Stanislav
 */

namespace task_24;

/**
 * Interface UserInterface
 */

interface UserInterface
{
    /**
     * function setName
     * @param $name
     *
     */
    public function setName($name); // установить имя

    /**
     * function getName
     *
     */
    public function getName(); // получить имя

    /**
     * gunction setAge
     * @param $age
     *
     */
    public function setAge($age); // установить возраст

    /**
     * function getAge
     *
     */
    public function getAge(); // получить возраст
}
