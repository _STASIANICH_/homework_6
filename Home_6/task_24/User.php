<?php
/**
 * Class User
 * @package task_24
 * @author Pechenyi Stanislav
 */

declare(strict_types=1);

namespace task_24;

/**
 * Class User
 * @package task_24
 * @author Pechenyi Stanislav
 */
class User implements UserInterface
{
    /**
     * Property name
     * @var string
     */
    private $name;

    /**
     * Property age
     * @var int
     */
    private $age;

    /**
     * function setName
     * @param $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * function getName
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * function serAge
     * @param $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

    /**
     * function getAge
     * @return float
     */
    public function getAge(): float
    {
        return $this->age;
    }
}
