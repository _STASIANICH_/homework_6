<?php

namespace task_33;

trait Trait3
{
    /**
     * @return float
     */
    private function method3(): float
    {
        return 3;
    }
}
