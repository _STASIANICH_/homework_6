<?php

/**
 * Class  Num.php
 *
 * @package task_23
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_23;

/**
 * Class Num
 *
 * @package task_23
 */
class Num
{
    /**
     *
     */
    const FIRSTCONST = 21;
    /**
     *
     */
    const SECONDCONST = 99;

    /**
     * Property num1
     * @var float
     */
    public static $num1 = 2;

    /**
     * Property num2
     * @var float
     */
    public static $num2 = 3;

    /**
     * Property num3
     * @var float
     */
    public static $num3 = 3;

    /**
     * Property num4
     * @var float
     */
    public static $num4 = 5;

    /**
     * function getSum
     * @return float
     */
    public function getSum(): float
    {
        return self::$num1 + self::$num3;
    }

    /**
     * function getSecondConst
     * @return float
     */
    public function getSecondConst(): float
    {
        return self::SECONDCONST;
    }
}
