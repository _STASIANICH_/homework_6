<?php

/**
 * ArraySumHelper
 *
 * @package task_22
 * @author Pechenyi Stanislav
 * @version 1.0.0 (14/02/2020)
 */

declare(strict_types=1);

namespace task_22;

/**
 * Class ArraySumHelper
 *
 * @package task_22
 */
class ArraySumHelper
{
    /**
     * function getPow
     *
     * @param $arr
     * @param $power
     * @return mixed
     */
    public static function getPow($arr, $power)
    {
        foreach ($arr as $key => $element) {
            $arr[$key] = pow($arr[$key], $power);
        }
        return $arr;
    }

    /**
     * function getSum
     *
     * @param $arr
     * @return int|mixed
     */
    public static function getSum($arr): float
    {
        $sum = 0;
        foreach ($arr as $element) {
            $sum += $element;
        }
        return $sum;
    }
}
