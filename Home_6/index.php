<?php

/**
 * Main file index.php
 * @author Pechenyi Stanislav
 * @version 1.1.1 (15/02/2020)
 */

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

/**Tasks 17.1 - 17.7*/

$arr = [];
$arr[] = new task_17\User('Kolia', 'Pupkin');
$arr[] = new task_17\Employee('Stas', 'Pechenyi', 200000);
$arr[] = new task_17\City('Kyiv', 3500000);

$arr[] = new task_17\User('Misha', 'Michael');
$arr[] = new task_17\Employee('Igor', 'Bailer', 3000);
$arr[] = new task_17\City('Cherkasy', 250000);

$arr[] = new task_17\User('Vasya', 'Vasiliev');
$arr[] = new task_17\Employee('Vania', 'Artemov', 1500);
$arr[] = new task_17\City('Odesa', 1700000);

/**Переберите циклом массив $arr и выведите на экран столбец
 * свойств name тех объектов, которые принадлежат классу User
 * или потомку этого класса.
 */
/*
foreach ($arr as $person) {
    if ($person instanceof task_17\User) {
        echo $person->name . '<br>';
    }
}

echo '-------------------<br>';
*/
/**Переберите циклом массив $arr и выведите на экран столбец
 * свойств name тех объектов, которые НЕ принадлежат классу User
 * или потомку этого класса.
 */
/*
foreach ($arr as $person) {
    if (!($person instanceof task_17\User)){
        echo $person->name . '<br>';
    }
}

echo '-------------------<br>';
*/
/**Переберите циклом массив $arr и выведите на экран столбец
 * свойств name тех объектов, которые принадлежат именно классу User,
 * то есть не классу City и не классу Employee
 */
/*
foreach ($arr as $person) {
    if (!($person instanceof task_17\City)
        && !($person instanceof task_17\Employee)) {
        echo $person->name . '<br>';
    }
}

echo '-------------------<br>';
*/
/**Tasks 18.1 - 18.9*/
/*
$programmer = new task_18\Post('programmer', 20000);
$manager = new task_18\Post('manager', 10000);
$driver = new task_18\Post('driver', 18000);

$employee1 = new task_18\Employee('Vasya', 'Pupkin', $programmer);

echo $employee1->getName() . '<br>';
echo $employee1->getSurname() . '<br>';
echo $employee1->post->getName() . '<br>';
echo $employee1->post->getSalary() . '<br>';
*/
/**Реализуйте в классе Employee метод changePost,
 * который будет изменять должность работника на другую
 */
/*
$employee1->changePost($driver);

echo $employee1->post->getName() . '<br>';
echo $employee1->post->getSalary() . '<br>';
*/
/**Tasks 19.1 - 19.6 доделать!*/
/*
$user1 = new task_19\User();
$user1->name = 'Kolia';
$user1->age = 25;
echo $user1->age;
echo '<br>';
$user1->setAgeV2(17);
echo $user1->age;
echo '<br>';
*/
/**Tasks 20.1 - 20.4 */
/*
$teacher1 = new task_20\Teacher();
$teacher1->transferToNextCourse();
$teacher1->setCourseAdministrator('Gadia', 'Hrenova','Petrovich');
$teacher1->getCourseAdministrator() . '<br>';
*/
/**Tasks 21.1 - 21.5*/
/*
$employee2 = new task_21\Employee('','','1998-03-23',30000);
echo $employee2->getAge() . '<br>';
*/
/**Tasks 22.1 - 22.4*/
/*
$arr = [1,2,3];

$arr = task_22\ArraySumHelper::getPow($arr, 2);
$sum = task_22\ArraySumHelper::getSum($arr);

echo $sum . '<br>';
*/
/**Tasks 23.1 - 23.4*/

/**
 * 23.1 Выведите сумму значений свойств на экран.
 */
/*
echo task_23\Num::$num1 + task_23\Num::$num2 . '<br>';
*/
/**
 * 23.3 Добавьте в класс Num метод getSum,
 * который будет выводить на экран сумму значений свойств num1 и num3.
 */
/*
$sum = new task_23\Num;
echo $sum->getSum() . '<br>';
*/
/**
 * 23.4 Выведите на экран значение первой константы напрямую,
 * и значение второй константы через геттер.
 */
/*
echo task_23\Num::FIRSTCONST . '<br>';
echo $sum->getSecondConst() . '<br>';
*/
/** Tasks 24.1 - 24.2*/
/*
$user2 = new task_24\User();
$user2->setName('Dima');
$user2->setAge(18);

echo $user2->getName() . '<br>';
echo $user2->getAge() . '<br>';
*/
/** Tasks 3.1 - 3.3 */
/*
$employee6 = new task_9\Employee();
$employee6->setAge(2);
echo $employee6->getSalary() . '<br>';

$teacher1 = new task_20\Teacher();
$teacher1->transferToNextCourse();
$teacher1->setCourseAdministrator('Gadia', 'Hrenova','Petrovich');
$teacher1->getCourseAdministrator() . '<br>';

$employee2 = new task_21\Employee('','','1998-03-23',30000);
echo $employee2->getAge() . '<br>';
*/
/** Task 3.1 - 3.2 */

$class = new ReflectionClass('task_20\Student');

if ($class->getParentClass()) {
    echo '<pre>';
    print_r($class->getParentClass());
    echo '</pre><br>';
} else {
    echo 'Отсутствует родительский класс<br>';
}

echo '<pre>';
$method = new ReflectionMethod('task_9\Employee', 'isAgeCorrect');
print_r($method->getDocComment());
echo '</pre>';

$method1 = new ReflectionMethod('task_21\User', 'getName');

if ($method1->isPrivate()) {
    echo 'Метод ' . $method1->getName() . ' приватный!<br>';
} elseif ($method1->isProtected()) {
    echo 'Метод ' . $method1->getName() . ' защищенный!<br>';
} else {
    echo 'Метод ' . $method1->getName() . ' публичный!<br>';
}

/** Task 3.3 */

$reflectionMethod = new ReflectionMethod('task_9\Employee', 'isAgeCorrect');

echo '<pre>';
print_r($reflectionMethod->getDocComment());
echo '</pre>';

echo '<b>Параметры метода:</b><br>';
$reflectionParams = $reflectionMethod->getParameters();

foreach ($reflectionParams as $parameter) {
    echo 'Параметр: ' . $parameter->getName() . '<br>';
}

echo '<b>Возвращаемый тип:</b> ' . $reflectionMethod->getReturnType() . '<br>';

/** Tasks 4.1 - 4.1 */

$phpVersions = new task_4\PhpVersionList();

/** заполните ее данными (на ваше усмотрение), минимум 25 элементов */

$phpVersions->enqueue('Php 5.0.0');
$phpVersions->enqueue('Php 5.0.1');
$phpVersions->enqueue('Php 5.0.2');
$phpVersions->enqueue('Php 5.0.3');
$phpVersions->enqueue('Php 5.0.4');
$phpVersions->enqueue('Php 5.0.5');
$phpVersions->enqueue('Php 5.1.0');
$phpVersions->enqueue('Php 5.1.1');
$phpVersions->enqueue('Php 5.1.2');
$phpVersions->enqueue('Php 5.1.3');
$phpVersions->enqueue('Php 5.2.0');
$phpVersions->enqueue('Php 5.2.1');
$phpVersions->enqueue('Php 5.2.2');
$phpVersions->enqueue('Php 5.2.3');
$phpVersions->enqueue('Php 5.2.4');
$phpVersions->enqueue('Php 5.2.5');
$phpVersions->enqueue('Php 5.2.6');
$phpVersions->enqueue('Php 5.2.7');
$phpVersions->enqueue('Php 5.2.8');
$phpVersions->enqueue('Php 5.2.9');
$phpVersions->enqueue('Php 5.2.10');
$phpVersions->enqueue('Php 5.2.11');
$phpVersions->enqueue('Php 5.2.15');
$phpVersions->enqueue('Php 5.3.0');
$phpVersions->enqueue('Php 5.3.1');

/** достаньте несколько элементов */

echo 'Элемент очереди №3: ' . $phpVersions[2] . '<br>';
echo 'Элемент очереди №12: ' . $phpVersions[11] . '<br>';
echo 'Элемент очереди №18: ' . $phpVersions[17] . '<br>';
echo 'Элемент очереди №25: ' . $phpVersions[24] . '<br>';

/** добавьте еще несколько элементов */

$phpVersions->enqueue('Php 5.3.2');
$phpVersions->enqueue('Php 5.3.3');
$phpVersions->enqueue('Php 5.3.4');
$phpVersions->enqueue('Php 5.3.6');
$phpVersions->enqueue('Php 5.3.7');
$phpVersions->enqueue('Php 5.3.9');
$phpVersions->enqueue('Php 5.3.19');

/** определите (если есть такая возможность),
 * где теперь находится указатель (то есть текущее
 * положение последнего элемента).
 */

echo 'Крайний элемент очереди: ' . $phpVersions->getLastElement() . '<br>';

/** Tasks 5.1 - 5.1 */

/**
 * SPL function 'class_uses'
 * Example #1
 */

echo '<pre>';
print_r(class_uses(new task_33\Test));
echo '</pre>';

/**
 * SPL function 'class_uses'
 * Example #2
 */

foreach (class_uses(new task_33\Test) as $class_us) {
    if ('task_33\Trait1' == $class_us) {
        echo 'Применён трейт Trait1!' . '<br>';
    }

    if ('task_33\Trait2' == $class_us) {
        echo 'Применён трейт Trait2!' . '<br>';
    }

    if ('task_33\Trait3' == $class_us) {
        echo 'Применён трейт Trait3!' . '<br>';
    }
}

echo 'Сумма результатов методов из 3 трейтов: ' . task_33\Test::getSum() . '<br>';

/**
 * SPL function 'iterator_to_array'
 */

$flowers = [
    [
        'Название' => 'Розы красные',
        'Цена' => 150,
        'Количество' => 15
    ],
    [
        'Название' => 'Тюльпаны красные',
        'Цена' => 80,
        'Количество' => 25,
    ],
    [
        'Название' => 'Орхидеи',
        'Цена' => 800,
        'Количество' => 4
    ],
    [
        'Название' => 'Хризантемы',
        'Цена' => 45,
        'Количество' => 15
    ],
    [
        'Название' => 'Еустомы',
        'Цена' => 250,
        'Количество' => 21
    ]
];

$iterator3 = new ArrayIterator($flowers);

/**
 * SPL function 'iterator_to_array'
 * Example #1
 */

echo '<pre>';
print_r(iterator_to_array($iterator3, false));
echo '</pre>';

/**
 * SPL function 'iterator_to_array'
 * Example #2
 */

foreach (iterator_to_array($iterator3, true) as $iter) {
    echo '<pre>';
    print_r($iter);
    echo '</pre>';
}

/** Tasks 6.1 - 6.2 */

$products = [
    'Яблоко' => 'Фрукт',
    'Апельсин' => 'Фрукт',
    'Картофель' => 'Овощи',
    'Морковка' => 'Овощи'
];

$iterator1 = new ArrayIterator($products);

foreach ($iterator1 as $key => $value) {
    echo $key . " =>  " . $value . "<br>";
}

$iterator1->ksort();

echo '---------------------------<br>';

foreach ($iterator1 as $key => $value) {
    echo $key . " =>  " . $value . "<br>";
}

echo '---------------------------<br>';

$iterator1->seek(2);
echo 'Товар #3: ' . $iterator1->key() . ' => ' . $iterator1->current() . '<br>';

echo '---------------------------<br>';

/** Tasks 7.1 - 7.2 */

$flowers = [
    [
        'Название' => 'Розы красные',
        'Цена' => 150,
        'Количество' => 15
    ],
    [
        'Название' => 'Тюльпаны красные',
        'Цена' => 80,
        'Количество' => 25,
    ],
    [
        'Название' => 'Орхидеи',
        'Цена' => 800,
        'Количество' => 4
    ],
    [
        'Название' => 'Хризантемы',
        'Цена' => 45,
        'Количество' => 15
    ],
    [
        'Название' => 'Еустомы',
        'Цена' => 250,
        'Количество' => 21
    ]
];

$iterator2 = new RecursiveArrayIterator($flowers);

foreach (new RecursiveIteratorIterator($iterator2) as $key => $value) {
    echo $key . ": " . $value . "<br>";
}

$iterator2->asort();

echo '-------------------------------<br>';

foreach (new RecursiveIteratorIterator($iterator2) as $key => $value) {
    echo $key . ": " . $value . "<br>";
}

echo '-------------------------------<br>';

echo 'Всего количество цветов: ' . $iterator2->count() . '<br>';

/** Tasks 8.1 - 8.2 */

$directory = new DirectoryIterator('/home/stanislav/PhpstormProjects/StudyProject/Home_6');

echo '<br><b>Содежимое папки /home/stanislav/PhpstormProjects/StudyProject/Home_6</b><br><br>';
foreach ($directory as $content) {
    if ($content->isDir()) {
        echo 'Папка: ' . $content->getFileInfo() . '<br>';
    }
    if ($content->isFile()) {
        echo 'Файл: <b>' . $content->getFilename() . '</b> объёмом <b>' . $content->getSize() . '</b> байт<br>';
    }
}
